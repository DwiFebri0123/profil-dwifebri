package com.example.profildwifebri

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class HobbyActivity : AppCompatActivity() {

    lateinit var HobbyView: RecyclerView
    lateinit var HobbyAdapter: HobbyAdapter
    val list = ArrayList<HobbyData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hobby)

        HobbyView = findViewById(R.id.rvHobby)
        HobbyView.layoutManager = LinearLayoutManager(this)

        list.add(HobbyData("Membaca", "Menambah Pengetahuan"))
        list.add(HobbyData("Mendengarkan Musik", "Mengurangi rasa stress"))

        HobbyAdapter = HobbyAdapter(list)
        HobbyView.adapter = HobbyAdapter
    }
}