package com.example.profildwifebri

data class ProjectData (
    val judul: String?,
    val desc: String?,
    val url: String?,
)
