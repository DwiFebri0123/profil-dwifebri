package com.example.profildwifebri

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ProjectActivity : AppCompatActivity() {

    lateinit var projectView: RecyclerView
    lateinit var projectAdapter: ProjectAdapter
    val list = ArrayList<ProjectData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project)

        projectView = findViewById(R.id.rvProject)
        projectView.layoutManager = LinearLayoutManager(this)

        list.add(
            ProjectData("Profil Guru",
            "projek profil guru", "https://gitlab.com/DwiFebri0123/projek-profil-guru")
        )
        list.add(
            ProjectData("Projek 2",
                "projek 2", "https://gitlab.com/DwiFebri0123/projek2")
        )

        projectAdapter = ProjectAdapter(list)
        projectView.adapter = projectAdapter
    }
}